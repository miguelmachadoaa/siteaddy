import Vue from 'vue'
import VueRouter from 'vue-router'

import HelloWorld from '@/components/HelloWorld'
import Detalle from '@/components/Detalle'
import About from '@/components/About'
import Contacto from '@/components/Contacto'
import Propiedades from '@/components/Propiedades'
import Blog from '@/components/Blog'
import DetalleBlog from '@/components/DetalleBlog'

Vue.use(VueRouter)

export default new VueRouter({
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/:code',
      name: 'homecode',
      component: HelloWorld
    },
    {
      path: '/',
      name: 'home',
      component: HelloWorld
    },

    {
      path: '/detalle/:id',
      name: 'detalle',
      component: Detalle
    },

   

    {
      path: '/blog',
      name: 'blog',
      component: Blog
    },


    {
      path: '/blog/:id',
      name: 'detalleblog',
      component: DetalleBlog
    },

    

    {
      path: '/propiedades',
      name: 'propiedades',
      component: Propiedades
    },

    

    {
      path: '/about',
      name: 'about',
      component: About
    },

    

    {
      path: '/contacto',
      name: 'contacto',
      component: Contacto
    }
  ]
})
