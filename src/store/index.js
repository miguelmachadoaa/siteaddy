import Vue from 'vue'
import Vuex from 'vuex'

import axios from "axios";
import { URL_LOCAL } from "../config.js";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
    color:{
      primario:'#85C1E9',
      tercero:'#f2551c',
      secundario: '#EBF5FB',
      cuarto: '#f5f5f5',
      font: '#333',
    },
    code:0,
    contacto:null,
    inmueble:[],
    inmuebles:[],
    ciudades:[],
    blogs:[],
    blog:[],
    banners:[],
    usuario:[],
    configuracion:[],
    markers:[],
    center:[],
  },
  getters: {
    
  },
  mutations: {
    setInmuebles(state, inmuebles){
        state.ciudades=inmuebles.data.ciudades;
        state.inmuebles=inmuebles.data.inmuebles;
        state.usuario=inmuebles.data.userdata;
        state.configuracion=inmuebles.data.configuracion;
        state.color.primario=inmuebles.data.configuracion.color_primario;
        state.color.secundario=inmuebles.data.configuracion.color_secundario;
        state.color.font=inmuebles.data.configuracion.font_color;
        console.log('setInmuebles');

    },

    setContacto(state, contacto){
      state.contacto=contacto;
    },

    setCode(state, code){
      state.code=code;
    },

    setInmueble(state, inmueble){
      state.inmueble=inmueble;

      const marker = {
        lat: parseFloat(inmueble.latitud),
        lng: parseFloat(inmueble.longitud),
      };
      state.markers.push({ position: marker, title: inmueble.direccion });
      state.center = marker;


    },

    setBlog(state, blog){
      state.blog=blog;
    },


    setBlogs(state, blogs){
      state.blogs=blogs;
    },

    setBanners(state, banners){
      state.banners=banners;
    },

    setMap(state, map){
      state.map=map;
    },
  },
  actions: {
    getInmuebles(context, code){
      axios
      .get(URL_LOCAL+"api/inmueble/"+code)
      .then((response) => (
        context.commit('setInmuebles', response.data)
      ));

    },

    getBlogs(context, code){
      axios
      .get(URL_LOCAL+"api/blogs/listado?code="+code)
      .then((response) => (
        context.commit('setBlogs', response)
      ));

    },

    getBanners(context, code){
      axios
      .get(URL_LOCAL+"api/banners/listado?code="+code)
      .then((response) => (
        context.commit('setBanners', response)
      ));

    },
    getInmueble(context, id){
      axios
      .get(URL_LOCAL+"api/inmueble_id/" + id)
      .then((response) => (
        context.commit('setInmueble', response.data.data.inmueble)
      ));

    },


    getBlog(context, id){
      axios
      .get(URL_LOCAL+"api/blog_id/" + id)
      .then((response) => (
        context.commit('setBlog', response.data)
      ));

    },

    getCode(context, code){
     
      context.commit('setCode', code)

    },

    getMap(context, map){
     
      context.commit('setMap', map)

    },

    addContacto(context, form){
      axios
      .post(URL_LOCAL+"api/contact", form)
      .then((response) => (
        context.commit('setContacto', response.data)
      ));

    },




  },
  modules: {
    
  },

})